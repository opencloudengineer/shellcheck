# CI template for shellchecking!

ShellCheck : A shell script static analysis tool that gives warnings and suggestions for bash/sh shell scripts.

shellcheck stage is run as a __test__ stage upon git push.

since it is a test stage, it is __allowed to fail__ / __pass with warnings__.
for more info, check : https://www.shellcheck.net/

## Add it to project's .gitlab-ci.yml file as follows :

```yaml
include:
- project: 'opencloudengineer/shellcheck'
  file: '/ci.yml'

shellcheck:
  stage: test
  extends: .shellcheck
```

### and add 'test' among the list of names of stages that will run (in .gitlab-ci.yml) 
